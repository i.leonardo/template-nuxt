// import { set, toggle } from '@/utils/vuex'

export const strict = false

export const state = () => ({
  version: process.env.version,
  debug: process.env.NODE_ENV === 'development',

  // dark: false,
})

export const mutations = {
  // setDark: set('dark'),
  // toggleDark: toggle('dark'),
}

export const actions = {}
