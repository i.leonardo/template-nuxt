import objectPath from 'object-path'

// Check key exist Object
export const has = (obj, property) => {
  return objectPath.has(obj, property)
}

// Promise setTimeout
export const wait = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}
