import objectPath from 'object-path'

export const set = (property) => (store, payload) => {
  objectPath.set(store, property, payload)
}

export const objAssign = (property) => (store, payload) => {
  const val = Object.assign({}, objectPath.get(store, property), payload)
  objectPath.set(store, property, val)
}

export const toggle = (property) => (store) => {
  objectPath.set(store, property, !objectPath.get(store, property))
}
