// eslint-disable-next-line import/no-extraneous-dependencies
import { pt } from 'vuetify/lib/locale'

export default function () {
  return {
    lang: {
      locales: { pt },
      current: 'pt',
    },
  }
}
